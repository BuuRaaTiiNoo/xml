package com.epam;


import com.epam.Gson.JavaToJson;
import com.epam.Gson.JsonToJava;
import com.epam.xml.Parser;
import com.epam.xml.Unmarshalling;


public class Main {


    private static final String PATHTOXMLFILE = "C:\\Users\\Vladimir_Kadrov\\IdeaProjects\\xml\\src\\com\\epam\\test.xml";
    private static final String PATHTOJSONFILE = "C:\\Users\\Vladimir_Kadrov\\IdeaProjects\\xml\\src\\com\\epam\\test.json";
    private static final String PATHTOSTARTXMLFILE = "com/epam/shop.xml";

    public static void main(String[] args) throws Exception {

        Parser parser = new Parser();
        Unmarshalling unmarshalling = new Unmarshalling();
        JavaToJson javaToJson = new JavaToJson();
        JsonToJava jsonToJava = new JsonToJava();

        System.out.println("XML to java:");
        System.out.println(parser.parse(PATHTOSTARTXMLFILE).toString());

        System.out.println("\n\nJava to json:");
        javaToJson.toJson(parser.parse(PATHTOSTARTXMLFILE), PATHTOJSONFILE);

        System.out.println("\n\nJson to java:");
        System.out.println(jsonToJava.toJava(PATHTOJSONFILE).toString());

        System.out.println("\n\nJava to XML:");
        unmarshalling.unmarshal(jsonToJava.toJava(PATHTOJSONFILE), PATHTOXMLFILE);
    }
}
