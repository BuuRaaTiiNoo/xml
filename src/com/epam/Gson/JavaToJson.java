package com.epam.Gson;

import com.epam.adapter.LocalDateSerializer;
import com.epam.repository.Shop;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;

public class JavaToJson {

    public void toJson(Shop shop, String path) {

        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());

        Gson gson = gsonBuilder.create();
        String json = gson.toJson(shop);
        System.out.println(json);

        //Convert object to JSON string and save into a file directly
        try (FileWriter writer = new FileWriter(path)) {
            gson.toJson(shop, writer);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
