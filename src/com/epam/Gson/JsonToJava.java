package com.epam.Gson;

import com.epam.adapter.LocalDateSerializer;
import com.epam.repository.Shop;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileReader;

import java.io.IOException;
import java.io.Reader;
import java.time.LocalDate;

public class JsonToJava {

    public Shop toJava(String path) {

        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());

        Gson gson = gsonBuilder.create();

        try (Reader reader = new FileReader(path)) {

            // Convert JSON to Java Object
            return gson.fromJson(reader, Shop.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
