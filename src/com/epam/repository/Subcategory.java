package com.epam.repository;

import com.epam.model.Product;

import javax.xml.bind.annotation.XmlElement;


import java.util.ArrayList;
import java.util.List;

public class Subcategory {

    private List<Product> products = new ArrayList<>();

    @XmlElement(name = "product")
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        return "Subcategory{" +
                "products=" + products +
                "}";
    }
}
