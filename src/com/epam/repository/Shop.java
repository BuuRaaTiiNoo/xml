package com.epam.repository;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement()
public class Shop {
    private List<Category> categories = new ArrayList<>();


    @XmlElement(name = "category")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Category> getCategories() {
        return categories;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "categories=" + categories +
                "}";
    }
}
