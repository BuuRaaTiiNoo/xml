package com.epam.xml;

import com.epam.repository.Shop;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class Unmarshalling {

    public void unmarshal(Shop shop, String path) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Shop.class);
            Marshaller marshaller = jaxbContext.createMarshaller();

            File file = new File(path);

            marshaller.marshal(shop, file);
            marshaller.marshal(shop, System.out);
        } catch (JAXBException e) {
            System.err.print(e.getMessage());
        }
    }
}
