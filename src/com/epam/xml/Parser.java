package com.epam.xml;

import com.epam.model.Product;
import com.epam.repository.Category;
import com.epam.repository.Shop;
import com.epam.repository.Subcategory;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Parser {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public Shop parse(String path) throws XMLStreamException {

        List<Category> categories = null;
        List<Product> products = null;
        List<Subcategory> subcategories = null;
        String tagContent = null;
        Shop shop = null;
        Category category = null;
        Subcategory subcategory = null;
        Product product = null;

        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = factory.createXMLStreamReader(ClassLoader.getSystemResourceAsStream(path));

        while (reader.hasNext()) {
            int event = reader.next();

            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    if ("shop".equals(reader.getLocalName())) {
                        shop = new Shop();
                        categories = new ArrayList<>();
                        subcategories = new ArrayList<>();
                        products = new ArrayList<>();
                    }
                    if ("category".equals(reader.getLocalName())) {
                        category = new Category();
                    }
                    if ("subcategory".equals(reader.getLocalName())) {
                        subcategory = new Subcategory();
                    }

                    if ("product".equals(reader.getLocalName())) {
                        product = new Product();
                        product.setId(Integer.parseInt(reader.getAttributeValue(0)));
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    tagContent = reader.getText().trim();
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "category":
                            categories.add(category);
                            shop.setCategories(categories);
                            break;
                        case "subcategory":
                            subcategories.add(subcategory);
                            category.setSubcategories(subcategories);
                            break;
                        case "product":
                            products.add(product);
                            subcategory.setProducts(products);
                            break;

                        case "maker":
                            product.setMaker(tagContent);
                            break;

                        case "model":
                            product.setModel(tagContent);
                            break;

                        case "date":
                            product.setDate(LocalDate.from(dateTimeFormatter.parse(tagContent)));
                            break;

                        case "color":
                            product.setColor(tagContent);
                            break;

                        case "cost":
                            product.setCost(Integer.parseInt(tagContent));
                            break;

                        case "quantity":
                            product.setQuantity(Integer.parseInt(tagContent));
                            break;
                    }
                    break;
                case XMLStreamConstants.START_DOCUMENT:
                    categories = new ArrayList<>();
                    subcategories = new ArrayList<>();
                    products = new ArrayList<>();
                    break;
            }
        }
        reader.close();
        return shop;
    }
}
